DROP DATABASE IF EXISTS Yuka_Quiz;

CREATE DATABASE Yuka_Quiz;

USE Yuka_Quiz;

CREATE TABLE IF NOT EXISTS Yuka_Quiz (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    puntuacion INT NOT NULL,
    resultado VARCHAR(255) NOT NULL,
    fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO Yuka_Quiz	(nombre, puntuacion, resultado, fecha)
	VALUES 				();
    
SELECT * FROM Yuka_Quiz;