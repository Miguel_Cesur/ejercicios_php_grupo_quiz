# Ejercicios_PHP_Grupo_Quiz



## Proyecto // Quiz

En este proyecto nos vamos a embarcar en realizar un Quiz o formulario que nos ayudara a saber que tipo de Kaiju o Monstruo somos.

Entre las opciones finales posibles tendremos las posibilidades de ser un:

    - KAIJU DULCE.
    - KAIJU NOBLE.
    - KAIJU AVENTURERO.
    - KAIJU FEROZ.

Todo dependera de las diferentes respuestas que daremos en las 10 preguntas a la que nos enfrentaremos.


## Estructura de ficheros -- Administrador

De cara a una gestion de los ficheros que encontraremos en este proyecto podremos encontrarnos un directorio principal en el que encontraremos los siguientes archivos y subdirectorios:

    - Directorio "css": almacena los archivos de estilos .css.

    - Directorio "image": almacena las diferentes imagenes que usamos en las paginas.

    - Archivo "index.php": la pagina de inicio del proyecto, en ella ingresaremos nuestro nombre de usuario para la sesion en la que podremos contestar las preguntas.

    - Archivo "nombre.php": la pagina donde se encuentra la primera pregunta del cuestionario. Ademas en ella podremos encontrar diferentes metodos y funciones que utilizaremos para almacenar por ejemplo la informacion de la sesion o usuario en cookies.

    - Archivo "preguntaX.php": existen 9 archivos iguales a este. En ellos se indicara cada una de las 10 preguntas del Quiz, el tipo de pregunta o contestacion que habra en ella y la forma en la que se puntua la anterior a ella.

    - Archivo "resultadosQuiz.php": en este archivo, se valorara la suma de puntos tras la contestacion de todas las preguntas y se dara por resultado que tipo de Kaiju eres en base a las respuestas optenidas y la forma de puntuar estas.


## Guia de uso para el usuario

De acuerdo a todo lo mencionado anteriormente, vamos a describiros las diferentes preguntas que nos encontraremos  en el test y el sistema de puntuaje que se considerara a la hora de almacenar la puntuacion para la resolucion del Quiz.

Pregunta 1:

    ¿Qué te gusta hacer en tu tiempo libre?

        - Destruir ciudades y luchar contra otros kaijus. [4 pts]
        - Explorar el mundo y descubrir lugares nuevos. [3 pts]
        - Proteger a la naturaleza y a los seres vivos. [2 pts]


Pregunta 2:

    ¿Qué tipo de comida prefieres?

        - Carne, mucha carne. [4 pts]
        - Pescado, marisco y algas. [3 pts]
        - Frutas, verduras y semillas. [2 pts]


Pregunta 3:

    ¿Cómo te llevas con los humanos?

        - Los odio y los considero una amenaza. [4 pts]
        - Los ignoro y los evito. [3 pts]
        - Los respeto y los ayudo. [2 pts]


Pregunta 4:

    ¿Qué poder especial te gustaría tener?

    - Lanzar rayos o fuego por la boca. [4 pts]
    - Volar o nadar a gran velocidad. [3 pts]
    - Controlar el clima o la mente. [2 pts]


Pregunta 5:

        ¿Qué tipo de música te gusta escuchar?

        - Rock, metal o punk. [4 pts]
        - Pop, reggaeton o electrónica. [3 pts]
        - Clásica, jazz o folk. [2 pts]


Pregunta 6:

    ¿Cuál sería tu hábitat ideal?

        - Una isla volcanica. [4 pts]
        - El fondo del oceano. [3 pts]
        - Una ciudad en ruinas. [2 pts]


Pregunta 7:

    ¿Qué es lo que más valoras?

        - Mi libertad. [4 pts]
        - Mi fortaleza. [3 pts]
        - Mi inteligencia. [2 pts]


Pregunta 8:

    ¿Qué te motiva más?

        - La venganza. [4 pts]
        - La conquista. [3 pts]
        - La supervivencia. [2 pts]


Pregunta 9:

    ¿Cómo te comportas cuando te enfrentas a un desafío?

        - Ataco con ferocidad. [4 pts]
        - Analizo la situación antes de actuar. [3 pts]
        - Utilizo mi resistencia para superar cualquier obstáculo. [2 pts]


Pregunta 10:

    ¿Cómo te relacionas con otros seres?

        - Ataco a todo lo que se cruza en mi camino. [4 pts]
        - Volar o nadar a gran velocidad. [3 pts]
        - Prefiero estar solo. [2 pts]


## Resolucion del Quiz

Una vez contestadas a las 10 preguntas o cuestiones formuladas en este Quiz tendremos la opcion de que se nos indique que tipo de Kaiju o Monstruo seríamos por lo que vamos a echar un repaso de nuevo a que opciones tenemos y en base a que puntuaje deberíamos de obtener para poder optar a ser cada uno de ellos.


- KAIJU DULCE:                                                  [Si tienes entre 10 y 19 puntos]
    Eres un monstruo adorable y amigable, que no busca el conflicto ni la destrucción.
    Te gusta la paz y la armonía, y te llevas bien con los humanos y otros kaijus.
    Tu kaiju ideal sería Baby Godzilla , el hijo de Godzilla, que es pequeño, tierno y juguetón.

- KAIJU NOBLE:                                                  [Si tienes entre 20 y 29 puntos]
    Eres un monstruo bondadoso y valiente, que defiende la justicia y el equilibrio. Te gusta la naturaleza y la vida, y te alías con los humanos y otros kaijus para protegerlos.
    Tu kaiju ideal sería Mothra, la reina de los monstruos, que es hermosa, poderosa y sabia.

- KAIJU AVENTURERO:                                             [Si tienes entre 30 y 39 puntos]
    Eres un monstruo curioso y audaz, que busca la diversión y la emoción. Te gusta el mundo y sus maravillas, y te relacionas con los humanos y otros kaijus para explorarlo
    Tu kaiju ideal sería Rodan, el rey de los cielos, que es rápido, ágil y leal.

- KAIJU FEROZ:                                                  [Si 40 o más puntos]
    Eres un monstruo temible y agresivo, que busca el poder y la dominación.
    Te gusta el caos y la destrucción, y te enfrentas a los humanos y otros kaijus para imponerte. 
    Tu kaiju ideal sería King Ghidorah, el rey de los terrores, que es malvado, cruel y despiadado.


## Authors and acknowledgment

Los autores del proyecto en este caso son Jean Claude Torres y Jose Miguel Luengo, alumnos de primera ano de Grado Superior Dual de Desarrollo de Aplicaciones Web, del centro Cesur Madrid, Plaza Eliptica.

## Project status

El proyecto se encuentra ya en estado final, listo para enviar a produccion.